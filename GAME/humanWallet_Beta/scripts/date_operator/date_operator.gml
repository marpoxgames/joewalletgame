
/**
* Returns the number of times the countdown fits into the time difference, or -1 if it does not fit.
*/

function date_second_spand_with_current_date(_date_last_update){
    return date_second_span(_date_last_update, date_current_datetime());
}

function countdown_gap_counter(_date_last_update, _countdown) {
    var _span = date_second_spand_with_current_date(_date_last_update);
    if (_span >= _countdown) {
        return floor(_span / _countdown);
    }
    return 0;
}

