function Item(_name, _desc, _spr, _price, _effect ) constructor {
    name = _name;
    description = _desc;
    sprite = _spr;
    price = _price;
    effect = _effect;
}

function Item_Food(_name, _desc, _spr, _price, _supply_value, _effect = function (){} ):
    Item(_name, _desc, _spr, _price, _effect ) constructor {
        is_food = true;
        supply_value = _supply_value;
}
