// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_push_item(_item, _quantity){
	with obj_gui_inventory{
        return inventory.add_item(_item,_quantity);
	}
}