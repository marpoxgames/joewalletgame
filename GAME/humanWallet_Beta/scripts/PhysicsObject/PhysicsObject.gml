function PhysicsObject(_instance, _clickable = false) constructor  {
    
    instance = _instance;
    
    clickable = _clickable;
    pressed = false;
    
    bool_ground = false;
    
    coord_x = 0;
    coord_y = 0;
    
    int_vx = 0;
    int_vx_max = 30;
    
    int_vy = 0;
    int_vy_max = 300;

    int_direction =1;
    
    int_gravity_acceleration = 50;
    int_ground_acceleration = 30;
    int_air_acceleration = 75;
    
    int_jump_height = 90;
    
    int_temporal_acceleration = -1;
    
    function update_physics(){
 
        check_collision_front()
        check_collision_ground()
        check_collision()
        check_collision_over_block()
        
        check_pressed()
        
        show_debug_message(string(coord_x) + " " + string(coord_y))
        
        return {
            x: coord_x, 
            y: coord_y, 
            direction: int_direction
        }
    }
    
    function check_collision_ground(){
        with(instance) {
    	   if(place_meeting(other.coord_x, other.coord_y+1, obj_block)){
                // GROUND 
                other.bool_ground = true;
                other.int_temporal_acceleration = other.int_ground_acceleration;
                return;
            }
            // AIR
            other.bool_ground = false;
            other.int_temporal_acceleration = other.int_air_acceleration;
            other.int_vy = scr_approach(other.int_vy, other.int_vy_max, other.int_gravity_acceleration);//Caida libre
        }
    }
    
    function check_collision_front(){ 
        with(instance) {
             if(!place_free(other.coord_x+(other.int_direction*8),other.coord_y-8))
             {
                 other.int_direction*=-1;
             }
        }
    }
    
    function check_collision_over_block(){
        with(instance) {
            if(place_meeting(other.coord_x, other.coord_y, obj_block) && 
                ( position_meeting(other.coord_x, other.coord_y, obj_block) )){
                other.coord_y -= 32
            }
        }
    }
    
    function check_collision(){
        var _coords = script_check_collision(instance, coord_x,coord_y,int_vx, int_vy);
        coord_x = _coords.x;
        coord_y = _coords.y;
    }
    
    function check_pressed(){
        if(clickable && pressed){
            int_vx = 0;
            int_vy = 0;
            coord_x = device_mouse_x(0);
            coord_y = device_mouse_y(0);
        }
    }
    
    function check_press(){
     
        if (!global.active_gui && 
            !global.active_pressed_something && 
            scr_control_press_left(coord_x, coord_y, 8)
            ){
                global.active_pressed_something=true;
                pressed=true;			 
        }
        
        if(pressed && scr_control_release_left(coord_x, coord_y, 8) )
            {	  
                global.active_pressed_something=false;
                pressed=false; 
            }
             
        
        
    }
    
    
    
}