// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function script_check_collision(_instance, _x, _y, _int_vx, _int_vy) {
    var _obj = obj_block;
    with (_instance) {
          	//Colision Horizontal 
      	repeat(abs(_int_vx)){
      		//Cuesta Arriba
      		if(place_meeting(_x  ,_y,_obj) 
                  && place_meeting(_x , _y-1,_obj) 
                  && place_meeting(_x , _y-2,_obj) 
                  && !place_meeting(_x , _y-3,_obj)){
      			_y-=3;
      		}else if(place_meeting(_x ,_y ,_obj) 
                  && place_meeting(_x ,_y-1 ,_obj) 
                  && !place_meeting(_x ,_y-2 ,_obj)){
      			_y-=2;
      		}else if(place_meeting(_x  ,_y ,_obj) 
                  && !place_meeting(_x  ,_y-1 ,_obj)){
      			_y--;
      		}
      		//Cuesta Abajo
      		if(!place_meeting(_x  ,_y,_obj) 
                  && !place_meeting(_x  , _y+1,_obj) 
                  && !place_meeting(_x+sign(_int_vx),_y+2,_obj) 
                  && !place_meeting(_x+sign(_int_vx),_y+3,_obj) 
                  && place_meeting(_x+sign(_int_vx),_y+4,_obj)){
      			_y+=3;
      		}else if(!place_meeting(_x  ,_y,_obj) 
                  && !place_meeting(_x  , _y+1,_obj) 
                  && !place_meeting(_x+sign(_int_vx),_y+2,_obj) 
                  && place_meeting(_x+sign(_int_vx),_y+3,_obj)){
      			_y+=2;
      		}else if(!place_meeting(_x  ,_y,_obj) 
                  && !place_meeting(_x  , _y+1,_obj) 
                  && place_meeting(_x+sign(_int_vx),_y+2,_obj)){
      			_y++;
      		}
      		//Colision Normal bloque
      		if(!place_meeting(_x, _y, _obj))
      		{
      		  _x += sign(_int_vx)*get_delta_time();
      		}else{
      		  _int_vx = 0;
      		break;
      	}};
      
      
      	//Colision Vertical 
      	repeat(abs(_int_vy)){
             	if(place_meeting(_x ,_y, _obj))
             	{
                  _int_vy = 0;
                  break;
             	}else{ 
             	     _y += sign(_int_vy)*get_delta_time(); 
             	}
          }     
    }


    return {x:_x , y:_y}
}


