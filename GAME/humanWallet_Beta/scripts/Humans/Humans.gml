enum HUMAN_STATES{
    IDLE,
    WALK,
    RUN,
    HUNGER,
    EAT,
    FALL_OUT,
    TAKED,
    DEAD,
    JUMP
}

enum HUMAN_NEED_STATES{
    DEAD ,
    BAD ,
    ACCEPT ,
    OK ,
    GOOD ,
    EXCELENT ,
}

function HumanNeed (_name, _state, _date, _countdown) constructor {
    name = _name;
    state = _state;
    max_state_value = HUMAN_NEED_STATES.EXCELENT;
    date_last_update = _date;
    countdown = _countdown;
    alert = false;
    
    function _change_state(_value){
    
        if(_value == 0){
            return;
        }
        
        state = _value;
        
        if (state > max_state_value){
            state = max_state_value;
        }
        if (state < 0){
            state = HUMAN_NEED_STATES.DEAD;
        }
        date_last_update = date_current_datetime();
    }
    
    function increase_state(_value) {
        var value = state + _value;
        _change_state(value)
    }
    
    function decrease_state(_value) {
        var value = state - _value;
        _change_state(value)

    }
    
    function refresh_state (_load=false){
        var _gap_counter = countdown_gap_counter(date_last_update, countdown)
		if(_gap_counter){
			decrease_state(_gap_counter);
		}
	
		if(_load){
            if(state <= 0){
                _change_state(HUMAN_NEED_STATES.BAD);
            }
        }
        
        if(state <= HUMAN_NEED_STATES.ACCEPT){
            alert = true;
        }else{
            alert = false
        }
    }
}

/// Human Class
function Human(_id, _name, _instance): PhysicsObject(_instance, true) constructor {
    uid = _id;
    name = _name;
    
    sprite = spr_joe_idle;
    state = HUMAN_STATES.IDLE;
    state_timer = 0;
    state_data = {};
    
    alert_happy = false;
    hungry = new HumanNeed(
                "hungry",
                HUMAN_NEED_STATES.ACCEPT, 
                date_current_datetime(),
                30
            );
    function update(){
        if(state != HUMAN_STATES.EAT){ // To Do var Ifpresseabled?
            check_press()
        }


        if(int_vy < 0){
             change_state(HUMAN_STATES.JUMP);
         }else{
             if(!bool_ground){
                change_state(HUMAN_STATES.FALL_OUT);
             }else{
                 if( state == HUMAN_STATES.FALL_OUT ){
                     change_state(HUMAN_STATES.IDLE);
                 }
             }
         }
         
                
        if(pressed){
            change_state(HUMAN_STATES.TAKED);
        }
        
        handle_state_machine_timer()
        handle_state_machine_action()
        handle_sprite()
    }
    
    function handle_state_machine_timer(){
        state_timer -= 1;
        switch (state) {
            case HUMAN_STATES.IDLE:
                if (state_timer <= 0) {
                    change_state(choose(HUMAN_STATES.WALK, 
                                        HUMAN_STATES.RUN)); // Transition to a new state
                }
                break;

            case HUMAN_STATES.WALK:
                if (state_timer <= 0) change_state(HUMAN_STATES.IDLE);
                break;
            
            case HUMAN_STATES.RUN:
                if (state_timer <= 0) change_state(HUMAN_STATES.IDLE);
                break;
            
            case HUMAN_STATES.HUNGER:
                if (state_timer <= 0) change_state(HUMAN_STATES.IDLE);
                break;
            
            case HUMAN_STATES.EAT:
                if (state_timer <= 0) change_state(HUMAN_STATES.IDLE);
                break;
        }
    }

    function handle_state_machine_action(){

        switch (state) {
            case HUMAN_STATES.IDLE:
                int_vx = 0;
            // TODO change direction
                break;

            case HUMAN_STATES.WALK:
                int_vx = scr_approach(int_vx, int_vx_max * int_direction, int_temporal_acceleration);
                break;
            
            case HUMAN_STATES.RUN:
                int_vx = scr_approach(int_vx, (int_vx_max * 3) * int_direction, int_temporal_acceleration);
                break;
            
            case HUMAN_STATES.HUNGER:
                int_vx = 0;
                break;
            
            case HUMAN_STATES.EAT:
                int_vx = 0;
                break;
        }
    }

    function handle_sprite(){
         switch (state) {
             case HUMAN_STATES.IDLE: 
                sprite = spr_joe_idle;
                break;
             case HUMAN_STATES.WALK: 
                sprite = spr_joe_walk;
                break;
             case HUMAN_STATES.RUN: 
                sprite = spr_joe_run;
                break;
             case HUMAN_STATES.HUNGER: 
                sprite = spr_joe_hunger;
                break;
             case HUMAN_STATES.EAT: 
                sprite = spr_joe_eat;
                break;
             case HUMAN_STATES.FALL_OUT: 
                sprite = spr_joe_fall_out;
                break;
             case HUMAN_STATES.TAKED: 
                sprite = spr_joe_taked;
                break;
             case HUMAN_STATES.DEAD: 
                sprite = spr_joe_hunger; // Usa otro sprite si necesario
                break;
             case HUMAN_STATES.JUMP: 
                sprite = spr_joe_taked;  // Usa otro sprite si necesario
                break;
         }
    }
    
    function change_state(_state) {
        state = _state;
        state_timer = irandom_range(30, 120); // Random duration for the state

        // Reset state-specific data
        state_data = {};
    } 
    
    function refresh_needs_states(_load){
        hungry.refresh_state(_load)
        
        if(hungry.state == HUMAN_NEED_STATES.DEAD){
            state = HUMAN_STATES.DEAD;
        }
        
        if(hungry.state >= HUMAN_NEED_STATES.GOOD){
            alert_happy = true
        }else{
            alert_happy = false 
        }
    }
    
    function eat(){
        if(!pressed && state != HUMAN_STATES.EAT){
            with(instance){
                var _instance_item = instance_place(other.coord_x, other.coord_y, obj_item_overworld);
				if(_instance_item && _instance_item.item.is_food == true){
					var _hungry_state = other.hungry.state
					if( _hungry_state < HUMAN_NEED_STATES.EXCELENT){
						if(other.state != HUMAN_STATES.EAT){
								other.change_state(HUMAN_STATES.EAT);
						}
                        
                        other.hungry.increase_state(_instance_item.item.supply_value)
                        
						instance_destroy(_instance_item);
						scr_generate_coins(other.coord_x,other.coord_y);
					}
				}
            }

		}	
	}
    
    function get_id(){
        return string(uid)
    }
    
    function save () {
        ini_open("maps-joe.ini");
            ini_write_string(get_id(), "name", name);
            ini_write_real(get_id(), "state", state);
            ini_write_string(get_id(), "hungry", base64_encode(json_stringify(hungry)));
        ini_close();
    }
    
    function load (_id) {
        uid = _id;
        
        ini_open("maps-joe.ini");
            name = ini_read_string(get_id(), "name", name);
            state = ini_read_real(get_id(), "state", state);
            hungry = json_parse(base64_decode(ini_read_string(get_id(), "hungry", base64_encode(json_stringify(hungry)))));
        ini_close();
        show_debug_message(hungry)
        hungry = new HumanNeed(hungry.name, hungry.state,hungry.date_last_update, hungry.countdown)

        refresh_needs_states(true)
    }
    
    function remove_data () {
        ini_open ("maps-joe.ini");
            ini_section_delete(get_id())
        ini_close ();
    }
    
    function destroy () {
        with(obj_ihud_humans){ 
            human_manager.delete_human_by_id(other.uid);
        }
        remove_data()
    }
}

/// HumanManager Class
function HumanManager(_max_humans) constructor {
    // Properties
    _limit_max_humans_alive_allowed = _max_humans;
    _current_humans_alive = 0;
    _total_humans_created = 0;
    _list_humans = []; // Array to store Human objects

    /// Create a human (respecting the limit)
    function _create_human() {
        if (_current_humans_alive <= _limit_max_humans_alive_allowed) {
            var _id = _total_humans_created;
            var _name = "joe_" + string(_id);
            var _new_human = new Human(_id, _name, noone);

            array_push(_list_humans, _new_human);
            _total_humans_created++;
            _current_humans_alive++;
            return _new_human; // Return the created human
        } else {
            show_message("Maximum limit of humans reached.");
            return undefined; // Cannot create
        }
    }
    
    function create_human() {

        var cx = (camera_get_view_x(view_camera[0])+ (global.width_camara/2))
        var cy = (camera_get_view_y(view_camera[0]))
        var _human = _create_human();
        if (_human != undefined ){
            with(instance_create_layer(cx,cy, "Instances", _obj_human_padre)){
                human = _human
                human.instance = self
                human.coord_x = cx;
                human.save();
            }
            save();            
        }
    }
    
    function load_humans() {
        var cx = (camera_get_view_x(view_camera[0])+ (global.width_camara/2))
        var cy = (camera_get_view_y(view_camera[0]))
        for (var i = 0; i < array_length(_list_humans); i++) {
            with(instance_create_layer(cx,cy, "instances", _obj_human_padre)){
                human.load(other._list_humans[i].uid)
                human.instance = self
                human.coord_x = cx;
            } 
        }
    }

    /// Get a human by ID
    function get_human_by_id(_id) {
        for (var i = 0; i < array_length(_list_humans); i++) {
            if (_list_humans[i].uid == _id) {
                return _list_humans[i];
            }
        }
        return undefined; // Not found
    }

    /// Delete a human by ID
    function delete_human_by_id(_id) {
        for (var i = 0; i < array_length(_list_humans); i++) {
            if (_list_humans[i].uid == _id) {
                array_delete(_list_humans, i, 1); // Remove from array
                _current_humans_alive--;
                save();
                return true; // Success
            }
        }
        return false; // Not found
    }

    /// List all humans (optional, for debugging)
    function list_humans() {
        return _list_humans;
    }
    
    function list_humans_to_string(){
        return json_stringify(_list_humans);
    }
    
    // Save the humans to a file
    function save() {
        ini_open("maps-joe.ini");
            ini_write_string("human-data", "_list_humans", base64_encode(list_humans_to_string()));
            ini_write_real("human-data", "_current_humans_alive", _current_humans_alive);
            ini_write_real("human-data", "_total_humans_created", _total_humans_created);
        ini_close();
    }

    // Load the humans from a file
    function load() {
        ini_open("maps-joe.ini");
            _list_humans = json_parse(base64_decode(ini_read_string("human-data", "_list_humans", base64_encode(list_humans_to_string()))));
            _current_humans_alive = ini_read_real("human-data", "_current_humans_alive", _current_humans_alive);
            _total_humans_created = ini_read_real("human-data", "_total_humans_created", _total_humans_created);
        ini_close();
    }
}



