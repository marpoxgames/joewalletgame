// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function yd_lang_next() {
	// shift to the next available locale languaje
	// used in the main menu to change lenguaje 

	global.yd_lang_idx++;
	if(global.yd_lang_idx>= array_length(global.yd_languajes))
	{
		global.yd_lang_idx=0;
	}

	global.yd_locale_words = ds_map_find_value(global.yd_locale_map,global.yd_languajes[global.yd_lang_idx]);




	global.game_language=global.yd_languajes[global.yd_lang_idx];
	show_debug_message("languaje is now " + global.game_language);
	//---------------GUARDO EL NUEVO LENGUAGE REGISTRADO------------------------------
			ini_open("game_settings.ini");
			ini_write_string("settings-language","language",global.game_language);
			ini_close();
	//--------------------------------------------------------------------------------



}
