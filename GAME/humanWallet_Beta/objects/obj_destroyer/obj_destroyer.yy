{
  "$GMObject":"",
  "%Name":"obj_destroyer",
  "eventList":[],
  "managed":true,
  "name":"obj_destroyer",
  "overriddenProperties":[],
  "parent":{
    "name":"GAME LEVELS",
    "path":"folders/Objects/GAME LEVELS.yy",
  },
  "parentObjectId":null,
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprite_destroyer",
    "path":"sprites/sprite_destroyer/sprite_destroyer.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}