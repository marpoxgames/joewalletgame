/// --- Movement Variables --- ///
/// @desc Vertical speed of the object, initialized to 0.
int_vy = 0;

/// @desc Maximum vertical speed, representing gravity's effect.
int_vy_max = 300;

/// @desc Gravity increment (or friction) applied to the object.
int_gravity_normal = 50;

/// @desc Boolean indicating whether the object is touching the ground.
bool_is_in_ground = scr_check_ground();