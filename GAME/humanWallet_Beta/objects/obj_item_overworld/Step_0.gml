// Update the item's sprite
sprite_index = item.sprite;

// --- Controls ---
#region ITEM CONTROLS

// If the item is not "empty"
    scr_taked_obj(); // Execute custom logic when an object is taken

    if (pressed) {
        // Disable movement and make the item follow the mouse
        int_vx = 0;
        int_vy = 0;
        x = device_mouse_x(0);
        y = device_mouse_y(0);
    } else {
        // Inherit gravity and default behavior when not pressed
        event_inherited();
    }

    // Handle right mouse button press
    if (device_mouse_check_button_pressed(0, mb_right) &&
        !global.active_pressed_something &&
        !global.active_taked_something) {

        // Check if the mouse is within the item's bounds
        if ((device_mouse_y(0) > y - 8 && device_mouse_y(0) < y + 8) &&
            (device_mouse_x(0) > x - 8 && device_mouse_x(0) < x + 8)) {

            global.active_taked_something = true;

            // Add the item to the inventory and destroy it if successful
            if (scr_push_item(item, 1)) {
                instance_destroy();
                global.active_taked_something = false;
            }
        }
    }


#endregion