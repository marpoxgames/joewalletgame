{
  "$GMObject":"",
  "%Name":"obj_control_settings",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"obj_control_settings",
  "overriddenProperties":[],
  "parent":{
    "name":"SETTINGS",
    "path":"folders/Objects/CONTROL/SETTINGS.yy",
  },
  "parentObjectId":{
    "name":"obj_control_base",
    "path":"objects/obj_control_base/obj_control_base.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_control_settings",
    "path":"sprites/spr_control_settings/spr_control_settings.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}