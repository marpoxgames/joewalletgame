{
  "$GMObject":"",
  "%Name":"obj_ihud_sound",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":3,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":64,"eventType":8,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"obj_ihud_sound",
  "overriddenProperties":[],
  "parent":{
    "name":"InterativeHUD",
    "path":"folders/Objects/CONTROL/GUI CONTROL/InterativeHUD.yy",
  },
  "parentObjectId":{
    "name":"obj_control_gui",
    "path":"objects/obj_control_gui/obj_control_gui.yy",
  },
  "persistent":true,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_control_ihud",
    "path":"sprites/spr_control_ihud/spr_control_ihud.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}