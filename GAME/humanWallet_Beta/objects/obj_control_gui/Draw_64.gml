event_inherited()

if( global.active_gui
	&& !active_gui_control){
	depth=1000
	
	if(global.active_wallet_game && current_gui_sprite){
		draw_sprite( current_gui_sprite , current_state , coord_button_x , coord_button_y );
	}

	return;
}else{
	depth=-1000
	scr_draw_black_gui_surface(0.5)
	if(global.active_wallet_game && current_gui_sprite){
		draw_sprite( current_gui_sprite , current_state , coord_button_x , coord_button_y );
	}
	scr_draw_black_gui_surface(0.5)
}

