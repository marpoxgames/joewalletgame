// INVENTORY OBJECT: DRAW GUI EVENT
// Draws the inventory UI, slots, and items

event_inherited();


draw_set_font(default_font);
draw_set_color(default_color);

// Draw inventory instructions
if (active_gui_control) {
    var _view_x = camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0]) / 9);
    var _view_y = camera_get_view_y(view_camera[0]) + ((camera_get_view_height(view_camera[0]) * 3) / 4);

    draw_line_width_color(_view_x, _view_y - 10, _view_x + (camera_get_view_width(view_camera[0]) / 2), _view_y - 10, 0.5, default_color, c_gray);

    var _instruction_text = global.active_pressed_something
        ? yd_lang("inventaryinstruction1PressSomething")
        : yd_lang("inventaryinstruction1NOTPressSomething");

    draw_text_ext_transformed(_view_x, _view_y, _instruction_text, 15, 300, text_scale, text_scale, 0);
}

// Draw inventory slots
if (active_gui_control) {
    var _view_x = camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0]) / 9);
    var _view_y = camera_get_view_y(view_camera[0]) + (camera_get_view_height(view_camera[0]) / 7);
    var _row = 0;
    var _column = 0;
    var _slot_sprite = spr_inventory_slot;
    var _offset = sprite_get_width(spr_inventory_slot)/2;
    var _color = c_ltgray;
    
    for (var _slot_index = 0; _slot_index < int_slots_total; _slot_index++) {
        var _slot_x = _view_x + (int_slot_gap * _column);
        var _slot_y = _view_y + (int_slot_gap * _row);
        
        var array_inventory_slots = inventory.get_inventory_slots()
        var _slot = array_inventory_slots[_slot_index]
        
        // Interactions
        if(scr_mouse_over_rectangule(_slot_x,_slot_y,_offset)){
            if(scr_control_press_left(_slot_x,_slot_y,_offset)){
                // mouse vacio y slot lleno 
                if((mouse==0) && (struct_names_count(_slot))){
                    inventory.subtract_item(_slot.item , 1 , _slot_index);
                    mouse = _slot.item;	
                }
                // mouse lleno y slot vacio
                else if((mouse!=0) && (struct_names_count(_slot) == 0)){	
                    inventory.add_item(mouse , 1, _slot_index);
                    mouse = 0;
                    global.active_pressed_something=false;	
                }
                // mouse lleno y slot lleno 
                else if((mouse!=0) && (struct_names_count(_slot) !=0)){
 					 
                    if(_slot.item == mouse){
                        //ITEMS IGUALES
                        inventory.add_item(mouse, 1, _slot_index);
                        mouse=0;
                        global.active_pressed_something=false;
                    }else{
                        //ITEMS DIFERENTES
                        if(not inventory.slot_has_quantity(_slot.item, 2,_slot_index)){
                            inventory.subtract_item(_slot.item, 1,_slot_index);   
                            inventory.add_item(mouse, 1, _slot_index);
                            mouse = _slot.item;  
                        }
                    }			
                }
            }	
            
            
            _color = c_yellow;   
        }else{
            _color = c_ltgray;
        }
        
        // Draw slot background
        draw_sprite_ext(_slot_sprite, 1, _slot_x, _slot_y, 1, 1, 0, _color, 1);
        draw_sprite_ext(_slot_sprite, 0, _slot_x, _slot_y, 1, 1, 0, default_color, 1);

        // Draw item in slot if present
        if (struct_names_count(_slot)) {
            var _item_quantity = string(_slot.quantity)

            draw_sprite(_slot.item.sprite, 0, _slot_x, _slot_y);
            draw_text_ext_transformed(
                _slot_x - 4 - ((string_width_ext(_item_quantity, 15, 300) / 2) * text_scale),
                _slot_y + 4,
                _item_quantity,
                15, 300, text_scale, text_scale, 0
            );
        }
        

        // Update coordinates for next slot
        _column++;
        if (_column >= 7) {
            _row++;
            _column = 0;
        }
    }
}

// Draw mouse-held item
if (mouse != 0) {
    global.active_pressed_something=true;
    //dibujar item
    draw_sprite(mouse.sprite, 0, device_mouse_x(0), device_mouse_y(0));	
    //soltar item  
    if(mouse_check_button_pressed(mb_right)){
        show_debug_message(mouse)
        with(instance_create_layer(device_mouse_x(0),device_mouse_y(0),"Instances", 
            obj_item_overworld)){
                item = other.mouse;
            
        }
        global.active_pressed_something=false;
        global.active_gui=false;
        active_gui_control = false;				
        mouse = 0;		
    }
}

