// INVENTORY OBJECT: CREATE EVENT
// Initialize variables, load saved inventory data, and set up slots.

// Load inherited events
event_inherited();
current_gui_sprite = spr_inventory_box;
// Define GUI variables
default_font = fnt_debug_type_0;
default_color = c_white;
text_scale = 0.6;
int_slot_gap = 20; // Distance between slots



inventory = new Inventory()
inventory.load()

int_slots_total = inventory.get_slots_total()