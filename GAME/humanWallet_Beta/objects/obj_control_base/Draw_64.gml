// Check if DEBUG is enabled
if (global.active_show_debug_game) {
    // Draw the sprite if one is assigned
    if (sprite_index != -1) {
        draw_self(); // Draws the object's sprite
    }
    
    // Draw the object's name below the sprite
    var _text_x = x; // X-coordinate for the text
    var _text_y = y + sprite_height / 2 + 5; // Y-coordinate just below the sprite
    var _object_name = object_get_name(object_index); // Gets the object's name
    
    // Set the text color and draw the text
    draw_set_color(c_white); // Set the text color to white
    draw_text(_text_x, _text_y, _object_name); // Draw the object's name at the calculated position
}
