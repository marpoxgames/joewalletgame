//-----------DATE TIME -----------------------

global.game_timer_sec=0;
global.game_timer_min=0;
global.game_timer_hour=0;
global.game_timer_day=0;

game_timer_sec=0;
game_timer_min=0;
game_timer_hour=0;
game_timer_day=0;

ini_open("game_data.ini");
	global.game_timer_sec=ini_read_real("player-data","game_timer_sec",game_timer_sec);
	global.game_timer_min=ini_read_real("player-data","game_timer_min",game_timer_min);
	global.game_timer_hour=ini_read_real("player-data","game_timer_hour",game_timer_hour);
	global.game_timer_day=ini_read_real("player-data","game_timer_day",game_timer_day);
ini_close();