{
  "$GMObject":"",
  "%Name":"obj_control_settings_time",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":3,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"obj_control_settings_time",
  "overriddenProperties":[],
  "parent":{
    "name":"SETTINGS",
    "path":"folders/Objects/CONTROL/SETTINGS.yy",
  },
  "parentObjectId":{
    "name":"obj_control_settings",
    "path":"objects/obj_control_settings/obj_control_settings.yy",
  },
  "persistent":true,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_control_settings",
    "path":"sprites/spr_control_settings/spr_control_settings.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}