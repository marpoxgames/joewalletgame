// STORE OBJECT: DRAW GUI EVENT
// Draws store UI, items, and interactions

event_inherited();

draw_set_font(default_font);
draw_set_color(default_color);

// Draw coins
if (global.active_wallet_game) {
    var _coins_text = "";
    if (total_coins >= 1000000) {
        _coins_text = string(total_coins / 1000000) + "M";
    } else if (total_coins >= 1000) {
        _coins_text = string(total_coins / 1000) + "K";
    } else {
        _coins_text = string(total_coins);
    }

    var _coin_x = coord_button_x;
    var _coin_y = coord_button_y + 20;
    draw_text_ext_transformed(
        _coin_x - ((string_width_ext(_coins_text, 15, 100) / 2) * text_scale),
        _coin_y,
        _coins_text,
        15, 100, text_scale, text_scale, 0
    );
    draw_sprite_ext(spr_coins, image_index, _coin_x + (string_width_ext(_coins_text, 15, 100) * text_scale), _coin_y + 3, 1, 1, 0, default_color, 0.6);
}

// Draw store slots
var _view_width = camera_get_view_width(view_camera[0]);
var _view_height = camera_get_view_height(view_camera[0]);
var _view_x = camera_get_view_x(view_camera[0]) + (_view_width / 6);
var _view_y = camera_get_view_y(view_camera[0]) + (_view_height / 7);

if (active_gui_control) {
    var _row = 0;
    var _column = 0;
    var _items_length = array_length(items);
    mouse = 0;

    for (var _i = 0; _i < _items_length; _i++) {
        var _slot_x = _view_x + (int_slot_gap * _column);
        var _slot_y = _view_y + (int_slot_gap * _row);

        // Highlight slot
        var _slot_color = c_ltgray;
        if (!global.active_pop_up) {
            if (point_in_rectangle(device_mouse_x(0), device_mouse_y(0), _slot_x - 8, _slot_y - 8, _slot_x + 8, _slot_y + 8)) {
                mouse = _i;

                if (mouse_check_button_pressed(mb_right)) {
                    if (items[_i].price > total_coins) {
                        show_message_async(yd_lang("store_no_money"));
                    } else {
                        scr_push_item(items[_i], 1);
                        show_message_async(yd_lang("store_item_added"));
                        total_coins -= items[_i].price;

                        ini_open("game_data.ini");
                        ini_write_real("Store", "total_coins", total_coins);
                        ini_close();
                    }
                }

                _slot_color = c_yellow;
            } else {
                _slot_color = (items[_i].price > total_coins) ? c_red : c_ltgray;
            }
        }

        // Draw slot background and border
        draw_sprite_ext(spr_inventory_slot, 1, _slot_x, _slot_y, 1, 1, 0, _slot_color, 1);
        draw_sprite_ext(spr_inventory_slot, 0, _slot_x, _slot_y, 1, 1, 0, default_color, 1);

        // Draw item in slot
        if (items[_i].sprite != -1) {
            draw_sprite(items[_i].sprite, 0, _slot_x, _slot_y);
            draw_text_ext_transformed(
                _slot_x - ((string_width_ext(items[_i].name, 15, 300) / 2) * text_scale),
                _slot_y + 4,
                items[_i].name,
                15, 300, text_scale, text_scale, 0
            );
        }

        // Update slot position
        _column++;
        if (_column >= 2) {
            _row++;
            _column = 0;
        }
    }

    // Display selected item details
    var _details_x = _view_x + (_view_width / 6);
    var _details_y = _view_y;
    var _data = (mouse == 0) ? data_to_show : items[mouse];
    
    var _msg = yd_lang("store_item_name_label") + ": " + _data.name;
    draw_text_ext_transformed(_details_x, _details_y, _msg, 15, 250, text_scale, text_scale, 0);

    _msg = yd_lang("store_item_info_label") + ": " + _data.description;
    draw_text_ext_transformed(_details_x, _details_y + 30, _msg, 15, 250, text_scale, text_scale, 0);

    _msg = yd_lang("store_item_price_label") + ": " + string(_data.price) + " tokens - ";
    draw_text_ext_transformed(_details_x, _details_y + 60, _msg, 15, 250, text_scale, text_scale, 0);
    draw_sprite_ext(spr_coins, image_index, _details_x + (string_width_ext(_msg, 15, 250) * text_scale) + (sprite_get_width(spr_coins) / 3), _details_y + 63, 1, 1, 0, default_color, 0.6);
}
