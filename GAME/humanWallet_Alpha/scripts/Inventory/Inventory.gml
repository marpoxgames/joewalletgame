function ItemInventory(_item, _quantity, _slot_index) constructor
{
    // `_item` is passed directly as a structure
    item = _item; 
    quantity = _quantity;
    slot_index = _slot_index;
}

function Inventory(int_slots_total = 21) constructor {
    _int_slots_total = int_slots_total; // Total number of slots in the inventory
    _inventory_slots = array_create(_int_slots_total, {}); // Array of inventory slots



    // Private method: Add a new item to the inventory
    function _create_item(_item, _quantity, _slot_index) {
        var new_item = new ItemInventory(_item, _quantity, _slot_index);
        _inventory_slots[_slot_index] = new_item;
    }

    // Private method: Remove an item by its _slot_index
    function _delete_item(_slot_index) {
        _inventory_slots[_slot_index] = {};
    }
    
    function _add_item(_item, _quantity, _slot_index ){
        if(item_exists(_item, _slot_index)){
            _inventory_slots[_slot_index].quantity += _quantity;
        }else{
            _create_item(_item, _quantity, _slot_index);
        }
        save();
    }  
    
    function _subtract_item(_item, _quantity, _slot_index ){
        if(slot_has_quantity(_item, _quantity ,_slot_index)){
            _inventory_slots[_slot_index].quantity -= _quantity;
            if(_inventory_slots[_slot_index].quantity <= 0){
                _delete_item(_slot_index)
            }
            save();
            return true;
        }
        return false
    } 
    
    
    
    function get_item_slots(_item) {
        var _slots = []
        for (var i = 0; i < _int_slots_total; i++) {
            if(struct_names_count(_inventory_slots[i]) && _inventory_slots[i].item == _item){
                array_push(_slots, i)
            }
        } 
        return _slots
    }

    // Check if an item exists in the inventory
    function item_exists(_item, _slot_index = undefined) {
        if (_slot_index != undefined){
            return bool(struct_names_count(_inventory_slots[_slot_index]));
        }else{
            // If slot index is not defined - search in all the slots
            var _slots = get_item_slots(_item)
            if (array_length(_slots)){
                return true;    
            } 
        }
        return false
    }

    // Check if an item has the required quantity
    function slot_has_quantity(_item, _quantity, _slot_index ) {
        return _inventory_slots[_slot_index].item == _item && _inventory_slots[_slot_index].quantity >= _quantity;
    }
    


    
    // Push Item to any slot available
    function _push_item(_item, _quantity) {
        var _slots = get_item_slots(_item)
        if(array_length(_slots)){
            _add_item(_item, _quantity, _slots[0])
        }else{
            var array_inventory_slots = get_inventory_slots()
            for (var i = 0; i < _int_slots_total; i++) {
                if(!struct_names_count(array_inventory_slots[i])){
                    _add_item(_item, _quantity, i);
                    return 
                }
            }
        }
    }
    
    // Add or update an item in the inventory
    function add_item(_item, _quantity, _slot_index = undefined) {
        
        if ( _slot_index == undefined ){
            _push_item(_item,_quantity);
        }else{
            _add_item(_item, _quantity, _slot_index)
        }
    }

    // Subtract a quantity from an item or remove it if the quantity reaches zero
    function subtract_item(_item, _quantity, _slot_index = undefined) {
        
        if ( _slot_index == undefined ){
            var _slots = get_item_slots(_item);
            for (var i = 0; i < _slots; i++) {
                var can_subtract_item = _subtract_item(_item, _quantity, _slots[i]);
                if can_subtract_item {
                    return true
                }
            }
        }else{
            return _subtract_item(_item, _quantity, _slot_index);
        }
    }

    
 

    // Get the total number of slots
    function get_slots_total() {
        return _int_slots_total;
    }

    // Get the list of inventory slots
    function get_inventory_slots() {
        return _inventory_slots;
    }

    function inventory_slots_to_string() {
        return json_stringify(_inventory_slots);
    }

    // Save the inventory to a file
    function save() {
        var inventory_slots_encoded = base64_encode(inventory_slots_to_string());
            ini_open("game_data.ini");
            ini_write_string("inventory", "_inventory_slots", inventory_slots_encoded);
        ini_close();
    }

    // Load the inventory from a file
    function load() {
        ini_open("game_data.ini");
            var inventory_slots_encoded = ini_read_string("inventory", "_inventory_slots", base64_encode(inventory_slots_to_string()));
        ini_close();
        _inventory_slots = json_parse(base64_decode(inventory_slots_encoded));
    }
}


