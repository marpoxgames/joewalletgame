// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_pop_up_text(text, time ){
///draw_text_box(x,y,text,time)
	var _inst  = instance_create_depth( 0 , 0 , -10000 , obj_ihud_pop_up );
	_inst.text = text;
	_inst.time = time;

	return _inst;
}