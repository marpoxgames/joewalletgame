// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

	
	function scr_calculate_min(argument0, argument1){

		var lastDateTaked = argument0

		var valueMin = argument1;

		var diff = date_minute_span(lastDateTaked, date_current_datetime ());
		
		
			if(diff>=valueMin){
					return true;
			}
	
		//END
		return false;
	}
	
	
	function scr_show_diff_in_seg(argument0, argument1){

		var lastDateTaked = argument0

		var valueMin = argument1;

		var diff = date_minute_span(lastDateTaked, date_current_datetime ());
		diff = diff*60;
	
		
		var valueMinInSeg= valueMin*60;
		
		var res = valueMinInSeg - diff
		return res;
		//END
		
	}
	
	function scr_convertime_in_hourclock_with_seg(argument0){

		var time = argument0
		
		var horas 
		var minutos 
		var segundos 
				
						horas = ( (time/3600) - frac((time/3600)) )					
						minutos = ( ((time-(horas*3600))/60) - frac(((time-(horas*3600))/60)) )							
						segundos =  (  (time-((horas*3600) + (minutos*60))) - frac((time-((horas*3600) + (minutos*60)))) )
								

		var res=[];
		
		res[0]= horas;
		res[1]= minutos;
		res[2]= segundos;
		
		
		return res;
		//END
	
	}
	
	
	function scr_calculate_hour(argument0, argument1){

		var lastDateTaked = argument0

		var valueMin = argument1;

		var diff = date_hour_span(lastDateTaked, date_current_datetime ());
			if(diff>=valueMin){
					return true;
			}
	
		//END
		return false;
	}
	
	
	


