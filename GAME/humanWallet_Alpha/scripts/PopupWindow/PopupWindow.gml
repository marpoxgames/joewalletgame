function PopupWindow(_text, _width, _height, _instance) constructor {
    // Create a struct representing the popup window
	instance=_instance;
    text= _text                                // Message text
    width= _width                              // Popup width
    height= _height                            // Popup height
    coord_x= (display_get_gui_width() - _width) / 2  // Centered X position
    coord_y= (display_get_gui_height() - _height) / 2// Centered Y position
	
	
	button_coord_x = coord_x + (width/2); 
	button_coord_y = coord_y + height;
	button_offset_x = 16
	button_offset_y = 8

    button_text= "Accept"                      // Button label
    is_hovered= false                          // Tracks if mouse is over button


    // Method to draw the popup window
    function draw() {

		draw_set_font(FONT_DEBUG_TYPE_0)

        // Draw popup background
        draw_set_color(c_dkgrey);
        draw_rectangle(coord_x, coord_y, coord_x + width, coord_y + height, false);

        // Draw popup border
        draw_set_color(c_black);
        draw_rectangle(coord_x, coord_y, coord_x + width, coord_y + height, true);

        // Draw message text
        draw_set_color(c_black);
        draw_set_halign(fa_center);
        draw_set_valign(fa_middle);
        draw_text(coord_x + width / 2, coord_y + height / 3, text);

     

        // Draw button with hover effect
        draw_set_color(is_hovered ? c_fuchsia : c_gray);
        draw_rectangle(button_coord_x-button_offset_x, button_coord_y-button_offset_y, button_coord_x+button_offset_x, button_coord_y + button_offset_y, false);

        // Draw button text
        draw_set_color(c_white);
        draw_text(button_coord_x , button_coord_y , button_text);
    }

    // Method to handle logic updates
    function update() {
        // Check if the mouse is hovering over the button
        is_hovered = scr_mouse_over_rectangule_gui(button_coord_x, button_coord_y , button_offset_x  );

        // If the button is clicked, remove the popup
        if (is_hovered && scr_control_press_left_gui(button_coord_x, button_coord_y , button_offset_x )) {
			with(instance){
				instance_destroy(); // Destroy popup instance
			}
        }
    }
}
