/// @function                scr_check_if_exists_or_create_layer(_name);
/// @param {string} _name    layer name
/// @description             Check is the layer exists in the current room - if not then create the layer
function scr_check_if_exists_or_create_layer(_name){
    if(!layer_exists(_name)){
        layer_create(_name);
    }
}

/// @function                scr_check_if_exists_or_create_instance();
/// @description             Check is the object exists in the current room - if not then create the instance
function scr_check_if_exists_or_create_instance(_x ,_y ,_layer ,_obj ){
    scr_check_if_exists_or_create_layer(_layer);
    
    if(!instance_exists(_obj)){
        instance_create_layer(_x,_y,_layer,_obj);
    }
}