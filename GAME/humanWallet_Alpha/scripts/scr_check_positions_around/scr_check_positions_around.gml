function scr_check_positions_around(_x, _y, _offset, _obj) {
    return (
        position_meeting(_x - _offset, _y - _offset, _obj) ||
        position_meeting(_x + _offset, _y - _offset, _obj) ||
        position_meeting(_x - _offset, _y + _offset, _obj) ||
        position_meeting(_x + _offset, _y + _offset, _obj)
    );
}