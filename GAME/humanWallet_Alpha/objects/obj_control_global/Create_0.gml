/// Macros
#macro DEBUG true
#macro DEBUG_MOUSE true


#macro FRAME_SEC game_get_speed(gamespeed_fps)

#macro FONT_DEBUG_COLOR c_white
#macro FONT_DEBUG_TYPE_1 fnt_debug_type_1
#macro FONT_DEBUG_TYPE_0 fnt_debug_type_0


global.active_pressed_something=false;
global.active_taked_something=false;

global.active_gui=false;
global.active_pop_up=false;


//-------------------------------------------------------
//----CONTROL DE GUI STARTMENU---------------------------------------------------
global.active_wallet_game=false;

//-----------------------PLAYER DATA-----------------------------------
//-------------------------------------------------------------------
var _room_wallet_default = rm_game_wallet;
ini_open("game_data.ini");
// Lee el valor desde el archivo INI, si no existe usa el valor predeterminado
var _room_wallet_name = ini_read_string("player-data", "roomWallet", room_get_name(_room_wallet_default));
ini_close();
global.current_room = asset_get_index(_room_wallet_name);
//------------------------SEETINGS-----------------------------------
//-------------------------------------------------------------------
//----------------------  RESOLUTION SETTINGS ------------------------

resolution_width_default=731;
resolution_height_default=411;
resolution_active_full_screen_default=true;
ini_open("game_settings.ini");
	global.active_display_full_screen=ini_read_string("settings-resolution","full-screen",resolution_active_full_screen_default);
	global.width_display=ini_read_real("settings-resolution","width",resolution_width_default);
	global.height_display=ini_read_real("settings-resolution","height",resolution_height_default);
ini_close();
scr_resolution_config();
//-----------------------------------------------------------------------


//Create settings objects

instance_create_layer(0,0,"control",obj_control_settings_debugger);
instance_create_layer(0,0,"control",obj_control_settings_camera);
instance_create_layer(0,0,"control",obj_control_settings_language);
instance_create_layer(0,0,"control",obj_control_settings_sounds);
//Create interact 
instance_create_layer(0,0,"control",obj_item_manager);
instance_create_layer(0,0,"control",obj_gui_inventory);
instance_create_layer(0,0,"control",obj_gui_store);
//Create control objects
instance_create_layer(0,0,"control",obj_ihud_sound);
instance_create_layer(0,0,"control",obj_ihud_humans);

//create control GUI to final
instance_create_layer(0,0,"control",obj_control_gui);

instance_create_layer(0,0,"control",obj_start_menu);



