/// @description FullScreen
// You can write your code in this editor
if(global.active_display_full_screen){
	global.active_display_full_screen=false;
			
}else{
	global.active_display_full_screen=true;
}

ini_open("game_settings.ini");
ini_write_real("settings-resolution","full-screen",global.active_display_full_screen);
ini_close(); 
	
scr_resolution_config();
	