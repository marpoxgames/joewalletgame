// STORE OBJECT: CREATE EVENT
// Initialize variables, load saved data, and set up slots

event_inherited();

current_gui_sprite = spr_gui_store;
default_font = fnt_debug_type_0;
default_color = c_white;
text_scale = 0.8;
int_slot_gap = 32;
total_coins = 0;

// Store items data
items = []
var _names = struct_get_names(global.items_list)
for (var i = 0; i < array_length(_names); i++) {
    array_push(items, struct_get(global.items_list, _names[i]))
}




data_to_show = items[0];

// Load coins data
ini_open("game_data.ini");
total_coins = 12;
ini_close();
