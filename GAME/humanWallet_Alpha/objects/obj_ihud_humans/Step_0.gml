
if( global.active_gui || !global.active_wallet_game ){
	return
}

var _press_left = device_mouse_check_button_released(0,mb_left)
var _device_mouse_y = device_mouse_y_to_gui(0)
var _device_mouse_x = device_mouse_x_to_gui(0)

if(global.active_wallet_game &&  mouse==0){	
	if(_press_left){
		if(( (_device_mouse_y > coord_button_y-coord_button_offset) 
		&& (_device_mouse_y < coord_button_y+coord_button_offset)) &&
				((_device_mouse_x > coord_button_x-coord_button_offset) 
				&& (_device_mouse_x < coord_button_x+coord_button_offset)) ){   	   
			if(active_gui_control){
				global.active_show_human_stats=false;
				active_gui_control=false;	
				current_state=STATE.CLOSE;
			}else{
				global.active_show_human_stats=true; 
				active_gui_control=true; 
				current_state=STATE.OPEN;
			}
		}
	}
}


//Cuando no existan humanos -> mostrar add human
px = coord_button_x;
py = coord_button_y+32;
	
if(scr_control_press_left(px, py, coord_button_offset)){
	human_manager.create_human();
}
	

if(global.active_wallet_game && !load){
    human_manager.load_humans()
    load = true;
}

if(!global.active_wallet_game){
    load = false;
}

