/// @description Handles the global in-game time, updating seconds, minutes, hours, and days dynamically.

#region TIME GLOBAL INTO GAME

// Increment seconds using delta time
global.game_timer_sec += get_delta_time();

if (global.game_timer_sec >= 60) {
    global.game_timer_sec -= 60; // Reset seconds to the remaining fraction
    global.game_timer_min++;     // Increment minutes
}

// Update minutes and increment hours when needed
if (global.game_timer_min >= 60) {
    global.game_timer_min -= 60; // Reset minutes
    global.game_timer_hour++;    // Increment hours
}

// Update hours and increment days when needed
if (global.game_timer_hour >= 24) {
    global.game_timer_hour -= 24; // Reset hours
    global.game_timer_day++;      // Increment days
}

#endregion





ini_open("game_data.ini");
	ini_write_real("player-data","game_timer_sec",global.game_timer_sec);
	ini_write_real("player-data","game_timer_min",global.game_timer_min);
	ini_write_real("player-data","game_timer_hour",global.game_timer_hour);
	ini_write_real("player-data","game_timer_day",global.game_timer_day);
ini_close();