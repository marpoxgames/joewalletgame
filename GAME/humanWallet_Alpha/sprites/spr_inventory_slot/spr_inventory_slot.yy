{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_inventory_slot",
  "bbox_bottom": 15,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9d832e1c-b6f5-45b1-aba5-30f4bfab717d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"527b44c8-f060-41b8-9b07-f8733c02a406",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 16,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"334ef6ea-f0e8-4ac5-846f-8c474580e8d9","blendMode":0,"displayName":"default","isLocked":false,"opacity":30.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 4,
  "parent": {
    "name": "BOX",
    "path": "folders/Sprites/INVENTORY/BOX.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_inventory_slot",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 2.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"9d832e1c-b6f5-45b1-aba5-30f4bfab717d","path":"sprites/spr_inventory_slot/spr_inventory_slot.yy",},},},"Disabled":false,"id":"ca3085ac-3cee-4863-8ef8-d1da6c313d5f","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"527b44c8-f060-41b8-9b07-f8733c02a406","path":"sprites/spr_inventory_slot/spr_inventory_slot.yy",},},},"Disabled":false,"id":"e5a91ec3-a509-4efc-9da4-806c8e37e777","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 8,
    "yorigin": 8,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 16,
}