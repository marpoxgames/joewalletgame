{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_gui_store",
  "bbox_bottom": 31,
  "bbox_left": 1,
  "bbox_right": 30,
  "bbox_top": 2,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"30eee399-2d16-47c2-bf2d-0b2614498957",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8e010a5b-757e-408f-b551-86dbdf3fc866",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 32,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"e0341aa9-cc07-4923-92ba-82fc8b845b71","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 4,
  "parent": {
    "name": "GUI",
    "path": "folders/Sprites/GUI.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_gui_store",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 2.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"30eee399-2d16-47c2-bf2d-0b2614498957","path":"sprites/spr_gui_store/spr_gui_store.yy",},},},"Disabled":false,"id":"e31c2c63-4e26-4a61-b5fe-ce218078f21b","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"8e010a5b-757e-408f-b551-86dbdf3fc866","path":"sprites/spr_gui_store/spr_gui_store.yy",},},},"Disabled":false,"id":"df01d6ea-ba19-441b-a647-141fc7957d33","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 16,
    "yorigin": 16,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 32,
}