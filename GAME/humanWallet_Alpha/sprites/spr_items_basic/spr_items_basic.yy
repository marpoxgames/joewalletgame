{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_items_basic",
  "bbox_bottom": 15,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bboxMode": 1,
  "collisionKind": 4,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"44b27435-3a19-461c-9ba2-d45b3e99111a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e8e744ea-fcf6-48bc-b72d-0d45117a9d94",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5c5174dd-fa29-455d-a145-bf74ffe90433",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 16,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"f525c27a-95b0-4825-bbea-685c7891ef31","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 4,
  "parent": {
    "name": "ITEMS",
    "path": "folders/Sprites/INVENTORY/ITEMS.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_items_basic",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 3.0,
    "lockOrigin": true,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"44b27435-3a19-461c-9ba2-d45b3e99111a","path":"sprites/spr_items_basic/spr_items_basic.yy",},},},"Disabled":false,"id":"28cb73c6-5316-43bc-b979-c330e8585e08","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"e8e744ea-fcf6-48bc-b72d-0d45117a9d94","path":"sprites/spr_items_basic/spr_items_basic.yy",},},},"Disabled":false,"id":"afe7e6ee-04b0-447c-98c7-a7be30d2be41","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"5c5174dd-fa29-455d-a145-bf74ffe90433","path":"sprites/spr_items_basic/spr_items_basic.yy",},},},"Disabled":false,"id":"e3d03a4e-6a68-41e0-bf6f-e4997fe29ca6","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 8,
    "yorigin": 8,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 16,
}