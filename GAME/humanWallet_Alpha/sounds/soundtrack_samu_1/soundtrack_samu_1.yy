{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "soundtrack_samu_1",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 3,
  "conversionMode": 0,
  "duration": 117.053,
  "parent": {
    "name": "SOUNDTRACK",
    "path": "folders/Sounds/SOUNDTRACK.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "soundtrack_samu_1.mp3",
  "type": 2,
  "volume": 1.0,
}