/// @description DRAW needs&Stadistics
draw_set_font(FONT_DEBUG_TYPE_1)
draw_set_color(FONT_DEBUG_COLOR)

if(global.active_show_debug_game){
		draw_rectangle(x-2,y-6,x+2,y+6,true)
		draw_text_ext_transformed( x,y-40,human.name, 15, 300, 0.4, 0.4, 0);
		draw_text_ext_transformed( x,y-50,human.state, 15, 300, 0.4, 0.4, 0);
		draw_text_ext_transformed( x,y-60,statesAr[human.state], 15, 300, 0.4, 0.4, 0);
		draw_text_ext_transformed( x,y-70,"hungrystate"+string(human.hungry.state), 15, 300, 0.4, 0.4, 0);
		draw_text_ext_transformed( x,y-80 ,"date_last_updateYear: "+date_datetime_string(human.hungry.date_last_update), 15, 300, 0.4, 0.4, 0);


}




#region SPAM ALERT HUNGRY

			if(human.hungry.alert){
				draw_set_color(c_white);



					draw_sprite_ext(spr_gui_need_food,0,x,y-(16+intCountShowName),1,1,0,c_white,intAlphaShowName);
	
					intCountShowName=intCountShowName+0.5;
					intAlphaShowName=intAlphaShowName-0.01;
	
					draw_set_alpha(1)


					if(intAlphaShowName<=0){

							intCountShowName=0;
							intAlphaShowName=1;
								human.hungry.alert=-1;
					}
	
			}
#endregion


#region SPAM ALERT HAPPY
		if(human.alert_happy){
			draw_set_color(c_white);



	
			draw_sprite_ext(spr_gui_need_happy,0,x,y-(16+intCountShowName),1,1,0,c_white,intAlphaShowName);
	
			intCountShowName=intCountShowName+0.5;
			intAlphaShowName=intAlphaShowName-0.01;
	
			draw_set_alpha(1)


			if(intAlphaShowName<=0){

			intCountShowName=0;
			intAlphaShowName=1;
				human.alert_happy=-1;
			}
	
		}
#endregion


#region LIST STADISTICS NEEDS

		if(global.active_show_human_stats){

	
		var anchoM=32;
		var altoM=64;
		var angl=10;
		
				draw_roundrect_color_ext(x-anchoM,y-altoM,x+anchoM,y-16,angl,angl,c_white,c_white,0);
				draw_roundrect_color_ext(x-anchoM,y-altoM,x+anchoM,y-16,angl,angl,c_black,c_orange,1);


				draw_set_color(c_black)
				//NAME-y=5
				draw_text_ext_transformed( x-((string_width_ext(human.name,15,300)/2)*0.4),y-(altoM-5),human.name, 15, 300, 0.4, 0.4, 0);
				//NEEDS
				//FILA 1 hungy y=15
				draw_sprite_ext(spr_gui_need_food,0,(x-(anchoM/2)),y-(altoM-18),1,1,0,c_white,1);
				draw_text_ext_transformed( x-((string_width_ext("=>",15,300)/2)*0.4),y-(altoM-15),"=>", 15, 300, 0.4, 0.4, 0);
				draw_text_ext_transformed( (x+(anchoM/2))-((string_width_ext(string(human.hungry.state)+"/5",15,300)/2)*0.4),y-(altoM-15),string(human.hungry.state)+"/5", 15, 300, 0.4, 0.4, 0);
				
					var lastDateTaked = human.hungry.date_last_update;
					var time = scr_show_diff_in_seg(lastDateTaked  ,human.hungry.countdown)
					time = scr_convertime_in_hourclock_with_seg(time)
					var timestr = string(time[0]) + ":" + string(time[1]) + ":" + string( time[2] ) + "-> (-1)";
					draw_text_ext_transformed( x-((string_width_ext(timestr,15,300)/2)*0.3),y-(altoM-25),timestr, 15, 300, 0.3, 0.3, 0);
				
				//FILA 2 happy y=35
				if(human.hungry.state==5){
					draw_sprite_ext(spr_gui_need_happy,0,x,y-(altoM-35-(sprite_get_height(spr_gui_need_happy)/2)),1,1,0,c_white,1);
				}
		}

#endregion


