//-----------------------------------------------------
//-------------CONTROLES----------------
//-----------------------------------------------------
//IF PRESSED
if(human.state!=states.eat){ // To Do var Ifpresseabled?
scr_taked_obj()
}

#region INTERACTIONS
	if(!pressed){
				//HUNGRY
		#region TO EAT--hungry ++STATE
		if(human.state!=states.eat){
				var inst = instance_place(x, y, obj_item_overworld);
				if(inst && inst.item.is_food == true){
					var st = human.hungry.state
					if(((st>=0)  && (st<5) ) || ((st==5) && human.state==states.eat)){
						if(human.state!=states.eat){
								human.state=states.eat;
						}
						var add1 = (st+inst.item.supply_value)
						if(add1>=5){
							add1=5;
						}
						human.hungry.alert=false;
						instance_destroy(inst);
						scr_generate_coins(x,y);
                        human.hungry.state = add1;
					}
				}
		}	
		#endregion
 
	}
#endregion

#region STATE MACHINE AND ANIMATIONS

			//-----------------------------------------------------
			//-------------STATE MACHINE ----------------
			//-----------------------------------------------------

switch(human.state) {
    case states.idle:
        intVX = 0;
		alarm[1]=FRAME_SEC*random(intProbChangeState)+1; //STAR State
		alarm[2]=FRAME_SEC*random(intProbChangeState)+1; //STAR State  
        break;

    case states.walk:
        intVX = scr_approach(intVX, intVXMax * intDir, intTempAcc);
		alarm[0]=FRAME_SEC*random(intProbChangeState)+1; //STAR IDLE- STOP MOVEa cambiar estado
        break;

    case states.run:
        intVX = scr_approach(intVX, (intVXMax * 2) * intDir, intTempAcc);
		alarm[0]=FRAME_SEC*random(intProbChangeState)+1; //STAR IDLE- STOP MOVEo
        break;

    case states.hunger:
    case states.eat:
    case states.jump:
        intVX = 0;
        intVY = 0;
        break;

    case states.taked:
        intVX = 0;
        intVY = 0;
        x = device_mouse_x(0);
        y = device_mouse_y(0);
        break;

    case states.dead:
        intVX = 0;
        intVY = 0;
        with(obj_ihud_humans){
            human_manager.delete_human_by_id(other.human.uid);
        }
        human.remove_data()
        instance_destroy();
        break;
}
		//-----------------------------------------------------
		//----------HUNGER STATES--------------
		//-----------------------------------------------------

			var st = human.hungry.state
	
			switch(st){
					case 0:
						human.state= states.dead ;
						human.alert_happy=false;
					break;
					case 1:
						human.hungry.alert=true;
						human.alert_happy=false;
					break;
					case 2:
					if(human.state!= states.eat){
                        human.hungry.alert=true;
                        human.state= states.hunger ;

					}
					human.alert_happy=false;
					break;
					case 3:
						human.hungry.alert=false;
						human.alert_happy=false;
					break;
					case 4:
	
						human.hungry.alert=false;
						human.alert_happy=false;
					break;
					case 5:
						human.hungry.alert=false;
                        human.alert_happy=true
					break;
			}



			//-----------------------------------------------------
			//----------CHANGE DIRECTION CONTRA PARED--------------
			//-----------------------------------------------------
					if(!place_free(x+(intDir*8),y-8))
					{
						intDir*=-1;
					}
					//----------IMAGE SEGUN DIRECTION
					image_xscale=intDir;
			//-----------------------------------------------------
			//-----------STATE MACHINE ANIMATION ---------------
			//-----------------------------------------------------
			//-------------------



		switch(human.state){
					case states.idle:
					sprite_index= get_sprite(sprits.idle);

					break;
	
					//----------------------
					case states.walk:
					sprite_index=get_sprite(sprits.walk);
						
					break;
					//----------------------
					case states.run:
					sprite_index=get_sprite(sprits.run);
					
					break;
					//-------------------
		
					case states.taked:
					sprite_index=get_sprite(sprits.taked);
			
					break;
					//-------------------
		
					case states.hunger:
					var duracionLimit = 5;
	
					sprite_index=get_sprite(sprits.hunger);
	
	
					timeAnimaton = timeAnimaton + (delta_time/1000000);
					if(timeAnimaton>=duracionLimit){
						human.state=get_sprite(states.idle);
						timeAnimaton=0;
					}
					break;
					//-------------------
		
					case states.eat:
					 duracionLimit = 5;
	
					sprite_index=get_sprite(sprits.eat);
	
	
					timeAnimaton = timeAnimaton + (delta_time/1000000);
					if(timeAnimaton>=duracionLimit){
						human.state=(states.idle);
						timeAnimaton=0;
					}
					break;
					//-------------------
					case states.fall_out:
						sprite_index=get_sprite(sprits.fall_out);
					break;
					//-------------------
					case states.jump:
						sprite_index=get_sprite(sprits.jump);
					break;
		}
#endregion

	

//---------------------------------------------------------------------------------------
//----------------------------------COLISIONES-------------------------------------------
//---------------------------------------------------------------------------------------
#region COLISIONES
			if(!pressed ){ 
			
									//----SCRIPT COMPRUEBA SI HAY SUELO ABAJO-----------///
									var bolGround   = scr_check_ground();//Script comprueba el suelo 
									//----------------GRAVEDAD----------------
										if(!bolGround)
										{
											intVY = scr_approach(intVY, intVYMax, intGravityNorm);//Caida libre
										}
									//Definir Aceleracin y Friccion en funcion del medio
										if(!bolGround)
										{
											intTempAcc = intAirAcc;   //ACELERACION EN EL AIRE
										}else{
											intTempAcc = intGroundAcc; // ACELERACION EN EL SUELO
										}
   
											//-----------CAMBIO DE ESTADO A JUMP------------
											if(intVY<0){
												human.state= states.jump;
											}else{
												if(!bolGround){
												human.state= states.fall_out ;
												}else{
													if(human.state==states.fall_out){
														human.state=states.idle
													}
												}
											}
											var dx=2,dy=7;
										if(place_meeting(x,y,obj_block) && ( position_meeting(x-dx,y-dy,obj_block) || position_meeting(x+dx,y-dy,obj_block) || position_meeting(x-dx,y+dy,obj_block)  || position_meeting(x+dx,y+dy,obj_block)   )){
											human.state = states.jump;
											  y -= 32
										}else{

											  script_check_colision(intVX,intVY);
										}
	
				}else{
						human.state= states.taked
				}
#endregion






human.refresh_needs_states()