/// @description Handles game GUI rendering, pre-start state, debug data, and resolutions.
/// This script initializes variables dynamically, manages GUI rendering, and provides debugging tools.

/// --- Initialization of GUI Dimensions ---
var _width_gui = display_get_gui_width();
var _height_gui = display_get_gui_height(); // Dynamically fetch height

/// --- Dynamic Values ---
var _circle_radius = 20;                    // Radius for mouse pulse effect

/// --- Set Default Draw Properties ---
draw_set_font(FONT_DEBUG_TYPE_0);
draw_set_color(c_white);
draw_set_alpha(1);

/// --- DEBUG: MOUSE PRESS ---
#region Debug: Mouse Press
var _left = device_mouse_check_button_released(0, mb_left)
var _right = device_mouse_check_button_released(0, mb_right)

if( DEBUG_MOUSE && 
	(_left ||
	_right) &&
    !global.active_pressed_something &&
    !global.active_taked_something) {

    var _pulse_x = device_mouse_x_to_gui(0);
    var _pulse_y = device_mouse_y_to_gui(0);
	
	var _pulse_color = c_white
	
	if(_right){
		_pulse_color = c_red
	}
	
    draw_set_color(_pulse_color);
    draw_circle(_pulse_x, _pulse_y, _circle_radius, true);
}
#endregion

var _debug_width = 1024
var _debug_high = 768
var _debug_section_offset = (_debug_width / 3) * 2; // Offset for the debug section
var _font_debug_color = FONT_DEBUG_COLOR;   // Debug font color (set dynamically)
display_set_gui_size(1024,768);
if (DEBUG & (global.active_show_debug_game || active_show_debug_globals)) {
    draw_rectangle_color(0, 0, _debug_width, _debug_high,c_black,c_black,c_black,c_black, false);
}

/// --- DEBUG: GLOBAL VARIABLES ---
#region Debug: Global Variables

if (DEBUG & active_show_debug_globals) {
    draw_set_font(FONT_DEBUG_TYPE_0);
    draw_set_color(_font_debug_color);
    draw_set_alpha(1);

    // Render Global Debug Header
    draw_text(0, 0, "GLOBAL DEBUGS");

    // Display Resolution Information
    draw_text(0, 20, "RESOLUTION GLOBAL:");
    draw_text(0, 30, "Stored Display Size: " + string(global.width_display) + " x " + string(global.height_display));
    draw_text(0, 40, "Camera Size: " + string(global.width_camara) + " x " + string(global.height_camara));
    draw_text(0, 50, "Fullscreen Active: " + string(global.active_display_full_screen));

    // Display Game Data
    draw_text(_debug_section_offset, 0, "DATA GAME GLOBAL:");
    draw_text(_debug_section_offset, 10, "Game Started: " + string(global.active_wallet_game));
    draw_text(_debug_section_offset, 30, "Font GUI: " + string(global.font_type_gui));
    draw_text(_debug_section_offset, 40, "Game Language: " + string(global.game_language));
    draw_text(_debug_section_offset, 50, "Pressed Something: " + string(global.active_pressed_something));
    draw_text(_debug_section_offset, 60, "Taked Something: " + string(global.active_taked_something));
	draw_text(_debug_section_offset, 70, "Active GUI: " + string(global.active_gui));
	draw_text(_debug_section_offset, 80, "Active Pop UP: " + string(global.active_pop_up));
	draw_text(_debug_section_offset, 90, "Active show human stats: " + string(global.active_show_human_stats));
	draw_text(_debug_section_offset, 100, "Room: " + room_get_name(global.current_room) + " (#" + string(global.current_room) + ")");

    // Display Room Information
    draw_text(_debug_width / 3, 120, "Room: " + room_get_name(room) + " (#" + string(room) + ")");
    draw_text(_debug_width / 3, 130, "Time Played: " +
        string(global.game_timer_hour) + ":" +
        string(global.game_timer_min) + ":" +
        string(global.game_timer_sec) + " Days: " +
        string(global.game_timer_day));
}

#endregion

/// --- DEBUG: GAME INFORMATION ---
#region Debug: Game Information

if (DEBUG & global.active_show_debug_game) {
    draw_set_font(FONT_DEBUG_TYPE_0);
    draw_set_color(_font_debug_color);
    draw_set_alpha(1);

    // Display FPS and Resolution Info
    draw_text(0, 0, "DEBUG GAME:");
    draw_text(0, 10, "RESOLUTION:");
    draw_text(0, 20, "FPS: " + string(fps) +
        " | Real FPS: " + string(fps_real) +
        " | Game Speed (FPS): " + string(game_get_speed(gamespeed_fps)));
    draw_text(0, 30, "GUI Size: " +
        string(_debug_width) + " x " +
        string(_debug_high));
    draw_text(0, 40, "Camera Size: " +
        string(camera_get_view_width(view_camera[0])) + " x " +
        string(camera_get_view_height(view_camera[0])));
    draw_text(0, 50, "Display Size: " +
        string(display_get_width()) + " x " +
        string(display_get_height()));
    draw_text(0, 60, "Window Size: " +
        string(window_get_width()) + " x " +
        string(window_get_height()));
    draw_text(0, 70, "Window Scale: " +
        string(window_get_width() / window_get_height()));

    // Display Stored Resolution Info
    draw_text(5, 80, "RESOLUTION GLOBAL:");
    draw_text(5, 90, "Stored Display Size: " +
        string(global.width_display) + " x " +
        string(global.height_display));
    draw_text(5, 100, "Camera Size: " +
        string(global.width_camara) + " x " +
        string(global.height_camara));
    draw_text(5, 110, "Fullscreen Active: " + string(global.active_display_full_screen));

    // Display Current Date and Time
    draw_text(_debug_section_offset, 0, "DATA:");
    draw_text(_debug_section_offset, 10, "Date: " +
        string(current_day) + "/" +
        string(current_month) + "/" +
        string(current_year) + ".");
    var _day;
    switch (current_weekday) {
        case 0: _day = "Sunday"; break;
        case 1: _day = "Monday"; break;
        case 2: _day = "Tuesday"; break;
        case 3: _day = "Wednesday"; break;
        case 4: _day = "Thursday"; break;
        case 5: _day = "Friday"; break;
        case 6: _day = "Saturday"; break;
    }
    draw_text(_debug_section_offset, 20, "The time is " + string(current_hour) + ":" +
        string(current_minute) + "." + string(current_second));
    draw_text(_debug_section_offset, 30, "Today is " + _day + ".");
}

#endregion
display_set_gui_size(-1, -1);