if( global.active_gui){
	return
}
	
if(scr_control_press_left(coord_button_x , coord_button_y, coord_button_offset)){
	if(global.active_sound_music){
		global.active_sound_music=false;
		active_gui_control=false;	
		current_state=STATE.CLOSE;
	}else{
		global.active_sound_music=true;  
		active_gui_control=true;	
		current_state=STATE.OPEN;
	}   
	ini_open("game_settings.ini");
		ini_write_real("settings-sound","active_sound_music",global.active_sound_music);
	ini_close();
}