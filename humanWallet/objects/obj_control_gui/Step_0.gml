if(( global.active_gui
	&& !active_gui_control)
	|| !current_gui_sprite){
	return
}

if(global.active_wallet_game &&  mouse==0){	
	if(scr_control_press_left(coord_button_x, coord_button_y, coord_button_offset)){  	   
		if(active_gui_control){
			global.active_gui=false;
			active_gui_control=false;	
			current_state=STATE.CLOSE;
		}else{
			global.active_gui=true; 
			active_gui_control=true; 
			current_state=STATE.OPEN;
		}
	}
}
