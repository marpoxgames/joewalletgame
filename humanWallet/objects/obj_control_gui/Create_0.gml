/// @description Dynamically adjusts GUI coordinates and assigns positions to control objects based on screen size.
// --- Create Event ---
active_gui_control=false
coord_button_y = 0
coord_button_x = 0
coord_button_offset = 16
mouse = 0;
enum STATE {
	OPEN = 0,
	CLOSE = 1
	}
current_state = STATE.CLOSE
current_gui_sprite = -1
// Initialize surface variable
_active_surface = -1;

// Define font characters and add a font dynamically
var _char_set = " ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789";
global.font_type_gui = font_add_sprite_ext(spr_font_yellow_1, _char_set, true, 1);

// --- Calculate GUI Dimensions ---
var _gui_width = display_get_gui_width();
var _gui_height = display_get_gui_height();

// Define margins and spacing
var _margin = _gui_width * 0.05;  // 5% margin from edges
var _spacing_x = _gui_width * 0.2;  // Horizontal spacing (10% of width)
var _spacing_y = _gui_height * 0.2; // Vertical spacing (10% of height)

// Define control objects
var _gui_control_objects = [
    obj_ihud_sound,
    obj_gui_inventory,
    obj_gui_store,
	obj_ihud_humans
];

// --- Calculate Button Positions Dynamically ---
button_positions = [
    {x: _margin, y: _margin},                                 // Sound control
    {x: _gui_width - _margin, y: _margin},                    // Inventory control
    {x: _gui_width - _margin - _spacing_x, y: _margin},        // List human control
    {x: _gui_width - _margin, y: _margin + _spacing_y}         // Store control
];

// --- Assign Positions to Control Objects ---
for (var _i = 0; _i < array_length(_gui_control_objects); _i++) {
    if (instance_exists(_gui_control_objects[_i])) {
        with (_gui_control_objects[_i]) {
            coord_button_x = other.button_positions[_i].x;
            coord_button_y = other.button_positions[_i].y;
        }
    }
}
