/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

//-------REACCION SALTO, SI SE APROXIMA PLAYER
#region DROP COIN
    if(state_coin=="drop"){
		if(distance_to_point(x_origin,y_origin)<10  ){
			int_vy= -int_jump_height;
			int_vx = scr_approach(int_vx,int_vx_max*int_direction,int_air_acceleration);	
			
		}
		x += int_vx*get_delta_time();
	}



	if(bool_is_in_ground){
		state_coin = "ground"
	}
#endregion




//-----------------------------------------------------
//-------------CONTROLES----------------
//-----------------------------------------------------
#region CONTROLES - INTERACTION TO COIN 

		if(device_mouse_check_button_pressed(0,mb_right) &&  global.active_pressed_something==false  && global.active_taked_something==false){
			if(( (device_mouse_y(0) > y-10) && (device_mouse_y(0) < y+10)) &&  ((device_mouse_x(0) > x-10) && (device_mouse_x(0) < x+10)) ){
				global.active_taked_something=true;
				//AGREGAR COIN TO TOTAL COINS
				if(scr_agregar_coin(value_of_coin)){
					instance_destroy();
					global.active_taked_something=false;
				}
			}
		}
	

#endregion

//IF PRESSED
