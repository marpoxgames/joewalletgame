/// @description Handles game GUI rendering, pre-start state, debug data, and resolutions.
/// This script initializes variables dynamically, manages GUI rendering, and provides debugging tools.

/// --- Initialization of GUI Dimensions ---
var _width_gui = display_get_gui_width();
var _height_gui = display_get_gui_height(); // Dynamically fetch height

/// --- Dynamic Values ---
var _logo_scale = 1.5;                      // Scale for the start menu logo
var _logo_base_x = 160;                     // Base X position for logo
var _logo_base_y = 80;                      // Base Y position for logo
var _press_key_offset = 5;                  // Offset for press-to-continue animation
var _circle_radius = 20;                    // Radius for mouse pulse effect
var _debug_section_offset = (_width_gui / 3) * 2; // Offset for the debug section
var _font_debug_color = FONT_DEBUG_COLOR;   // Debug font color (set dynamically)

/// --- Set Default Draw Properties ---
draw_set_font(FONT_DEBUG_TYPE_0);
draw_set_color(c_white);
draw_set_alpha(1);


/// --- PRE-START GAME SCREEN ---
#region Pre-Start Game Screen

if (!global.active_wallet_game) {
    // Render Pre-Start Screen Graphics
    draw_set_alpha(int_alpha_start);
    draw_sprite_ext(
        spr_logo_menustart,
        0,
        _logo_base_x,
        _logo_base_y + (int_alpha_press_key * _press_key_offset),
        _logo_scale,
        _logo_scale,
        0,
        c_white,
        1
    );

    // Render Press-to-Continue Text
    draw_set_alpha(int_alpha_press_key);
    draw_text(
        (global.width_camara / 2) - (string_width(yd_lang("presstocontinue")) / 2),
        160, // Y position kept as an example; can be dynamic
        yd_lang("presstocontinue")
    );
}

#endregion

