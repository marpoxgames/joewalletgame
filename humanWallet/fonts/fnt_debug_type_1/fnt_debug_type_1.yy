{
  "resourceType": "GMFont",
  "resourceVersion": "1.0",
  "name": "fnt_debug_type_1",
  "AntiAlias": 1,
  "applyKerning": 0,
  "ascender": 0,
  "ascenderOffset": 3,
  "bold": false,
  "canGenerateBitmap": true,
  "charset": 0,
  "first": 0,
  "fontName": "anim_curve_icon_font",
  "glyphOperations": 5,
  "glyphs": {
    "32": {"character":32,"h":24,"offset":0,"shift":10,"w":10,"x":2,"y":2,},
    "33": {"character":33,"h":24,"offset":5,"shift":12,"w":3,"x":92,"y":80,},
    "34": {"character":34,"h":24,"offset":2,"shift":12,"w":8,"x":82,"y":80,},
    "35": {"character":35,"h":24,"offset":0,"shift":12,"w":13,"x":67,"y":80,},
    "36": {"character":36,"h":24,"offset":1,"shift":12,"w":10,"x":55,"y":80,},
    "37": {"character":37,"h":24,"offset":0,"shift":12,"w":13,"x":40,"y":80,},
    "38": {"character":38,"h":24,"offset":0,"shift":12,"w":13,"x":25,"y":80,},
    "39": {"character":39,"h":24,"offset":5,"shift":12,"w":3,"x":20,"y":80,},
    "40": {"character":40,"h":24,"offset":3,"shift":12,"w":7,"x":11,"y":80,},
    "41": {"character":41,"h":24,"offset":3,"shift":12,"w":7,"x":2,"y":80,},
    "42": {"character":42,"h":24,"offset":1,"shift":12,"w":11,"x":97,"y":80,},
    "43": {"character":43,"h":24,"offset":1,"shift":12,"w":11,"x":235,"y":54,},
    "44": {"character":44,"h":24,"offset":4,"shift":12,"w":5,"x":216,"y":54,},
    "45": {"character":45,"h":24,"offset":2,"shift":12,"w":8,"x":206,"y":54,},
    "46": {"character":46,"h":24,"offset":4,"shift":12,"w":4,"x":200,"y":54,},
    "47": {"character":47,"h":24,"offset":2,"shift":12,"w":9,"x":189,"y":54,},
    "48": {"character":48,"h":24,"offset":1,"shift":12,"w":11,"x":176,"y":54,},
    "49": {"character":49,"h":24,"offset":2,"shift":12,"w":6,"x":168,"y":54,},
    "50": {"character":50,"h":24,"offset":1,"shift":12,"w":11,"x":155,"y":54,},
    "51": {"character":51,"h":24,"offset":1,"shift":12,"w":10,"x":143,"y":54,},
    "52": {"character":52,"h":24,"offset":0,"shift":12,"w":12,"x":129,"y":54,},
    "53": {"character":53,"h":24,"offset":1,"shift":12,"w":10,"x":223,"y":54,},
    "54": {"character":54,"h":24,"offset":1,"shift":12,"w":11,"x":110,"y":80,},
    "55": {"character":55,"h":24,"offset":1,"shift":12,"w":11,"x":123,"y":80,},
    "56": {"character":56,"h":24,"offset":1,"shift":12,"w":11,"x":136,"y":80,},
    "57": {"character":57,"h":24,"offset":1,"shift":12,"w":11,"x":147,"y":106,},
    "58": {"character":58,"h":24,"offset":5,"shift":12,"w":3,"x":142,"y":106,},
    "59": {"character":59,"h":24,"offset":4,"shift":12,"w":4,"x":136,"y":106,},
    "60": {"character":60,"h":24,"offset":1,"shift":12,"w":11,"x":123,"y":106,},
    "61": {"character":61,"h":24,"offset":1,"shift":12,"w":11,"x":110,"y":106,},
    "62": {"character":62,"h":24,"offset":1,"shift":12,"w":11,"x":97,"y":106,},
    "63": {"character":63,"h":24,"offset":1,"shift":12,"w":11,"x":84,"y":106,},
    "64": {"character":64,"h":24,"offset":0,"shift":12,"w":13,"x":69,"y":106,},
    "65": {"character":65,"h":24,"offset":0,"shift":12,"w":13,"x":54,"y":106,},
    "66": {"character":66,"h":24,"offset":1,"shift":12,"w":11,"x":41,"y":106,},
    "67": {"character":67,"h":24,"offset":1,"shift":12,"w":12,"x":27,"y":106,},
    "68": {"character":68,"h":24,"offset":1,"shift":12,"w":11,"x":14,"y":106,},
    "69": {"character":69,"h":24,"offset":2,"shift":12,"w":10,"x":2,"y":106,},
    "70": {"character":70,"h":24,"offset":2,"shift":12,"w":10,"x":235,"y":80,},
    "71": {"character":71,"h":24,"offset":1,"shift":12,"w":11,"x":222,"y":80,},
    "72": {"character":72,"h":24,"offset":1,"shift":12,"w":11,"x":209,"y":80,},
    "73": {"character":73,"h":24,"offset":2,"shift":12,"w":9,"x":198,"y":80,},
    "74": {"character":74,"h":24,"offset":1,"shift":12,"w":10,"x":186,"y":80,},
    "75": {"character":75,"h":24,"offset":2,"shift":12,"w":11,"x":173,"y":80,},
    "76": {"character":76,"h":24,"offset":2,"shift":12,"w":9,"x":162,"y":80,},
    "77": {"character":77,"h":24,"offset":1,"shift":12,"w":11,"x":149,"y":80,},
    "78": {"character":78,"h":24,"offset":1,"shift":12,"w":11,"x":116,"y":54,},
    "79": {"character":79,"h":24,"offset":0,"shift":12,"w":12,"x":102,"y":54,},
    "80": {"character":80,"h":24,"offset":1,"shift":12,"w":11,"x":89,"y":54,},
    "81": {"character":81,"h":24,"offset":0,"shift":12,"w":12,"x":37,"y":28,},
    "82": {"character":82,"h":24,"offset":1,"shift":12,"w":12,"x":15,"y":28,},
    "83": {"character":83,"h":24,"offset":1,"shift":12,"w":11,"x":2,"y":28,},
    "84": {"character":84,"h":24,"offset":1,"shift":12,"w":11,"x":232,"y":2,},
    "85": {"character":85,"h":24,"offset":1,"shift":12,"w":11,"x":219,"y":2,},
    "86": {"character":86,"h":24,"offset":0,"shift":12,"w":13,"x":204,"y":2,},
    "87": {"character":87,"h":24,"offset":0,"shift":12,"w":13,"x":189,"y":2,},
    "88": {"character":88,"h":24,"offset":0,"shift":12,"w":13,"x":174,"y":2,},
    "89": {"character":89,"h":24,"offset":0,"shift":12,"w":13,"x":159,"y":2,},
    "90": {"character":90,"h":24,"offset":1,"shift":12,"w":11,"x":146,"y":2,},
    "91": {"character":91,"h":24,"offset":4,"shift":12,"w":6,"x":29,"y":28,},
    "92": {"character":92,"h":24,"offset":2,"shift":12,"w":9,"x":135,"y":2,},
    "93": {"character":93,"h":24,"offset":3,"shift":12,"w":6,"x":114,"y":2,},
    "94": {"character":94,"h":24,"offset":1,"shift":12,"w":11,"x":101,"y":2,},
    "95": {"character":95,"h":24,"offset":-1,"shift":12,"w":14,"x":85,"y":2,},
    "96": {"character":96,"h":24,"offset":4,"shift":12,"w":5,"x":78,"y":2,},
    "97": {"character":97,"h":24,"offset":1,"shift":12,"w":10,"x":66,"y":2,},
    "98": {"character":98,"h":24,"offset":1,"shift":12,"w":11,"x":53,"y":2,},
    "99": {"character":99,"h":24,"offset":1,"shift":12,"w":11,"x":40,"y":2,},
    "100": {"character":100,"h":24,"offset":1,"shift":12,"w":11,"x":27,"y":2,},
    "101": {"character":101,"h":24,"offset":1,"shift":12,"w":11,"x":14,"y":2,},
    "102": {"character":102,"h":24,"offset":1,"shift":12,"w":11,"x":122,"y":2,},
    "103": {"character":103,"h":24,"offset":1,"shift":12,"w":11,"x":51,"y":28,},
    "104": {"character":104,"h":24,"offset":1,"shift":12,"w":11,"x":185,"y":28,},
    "105": {"character":105,"h":24,"offset":2,"shift":12,"w":10,"x":64,"y":28,},
    "106": {"character":106,"h":24,"offset":1,"shift":12,"w":8,"x":68,"y":54,},
    "107": {"character":107,"h":24,"offset":2,"shift":12,"w":11,"x":55,"y":54,},
    "108": {"character":108,"h":24,"offset":1,"shift":12,"w":11,"x":42,"y":54,},
    "109": {"character":109,"h":24,"offset":0,"shift":12,"w":12,"x":28,"y":54,},
    "110": {"character":110,"h":24,"offset":1,"shift":12,"w":11,"x":15,"y":54,},
    "111": {"character":111,"h":24,"offset":1,"shift":12,"w":11,"x":2,"y":54,},
    "112": {"character":112,"h":24,"offset":1,"shift":12,"w":11,"x":235,"y":28,},
    "113": {"character":113,"h":24,"offset":1,"shift":12,"w":11,"x":222,"y":28,},
    "114": {"character":114,"h":24,"offset":2,"shift":12,"w":10,"x":210,"y":28,},
    "115": {"character":115,"h":24,"offset":2,"shift":12,"w":9,"x":78,"y":54,},
    "116": {"character":116,"h":24,"offset":1,"shift":12,"w":10,"x":198,"y":28,},
    "117": {"character":117,"h":24,"offset":1,"shift":12,"w":11,"x":172,"y":28,},
    "118": {"character":118,"h":24,"offset":0,"shift":12,"w":12,"x":158,"y":28,},
    "119": {"character":119,"h":24,"offset":-1,"shift":12,"w":14,"x":142,"y":28,},
    "120": {"character":120,"h":24,"offset":1,"shift":12,"w":11,"x":129,"y":28,},
    "121": {"character":121,"h":24,"offset":0,"shift":12,"w":12,"x":115,"y":28,},
    "122": {"character":122,"h":24,"offset":1,"shift":12,"w":10,"x":103,"y":28,},
    "123": {"character":123,"h":24,"offset":2,"shift":12,"w":9,"x":92,"y":28,},
    "124": {"character":124,"h":24,"offset":5,"shift":12,"w":3,"x":87,"y":28,},
    "125": {"character":125,"h":24,"offset":2,"shift":12,"w":9,"x":76,"y":28,},
    "126": {"character":126,"h":24,"offset":1,"shift":12,"w":11,"x":160,"y":106,},
    "9647": {"character":9647,"h":24,"offset":4,"shift":20,"w":13,"x":173,"y":106,},
  },
  "hinting": 0,
  "includeTTF": false,
  "interpreter": 0,
  "italic": false,
  "kerningPairs": [],
  "last": 0,
  "lineHeight": 0,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "pointRounding": 0,
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "sdfSpread": 8,
  "size": 16.0,
  "styleName": "Regular",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "TTFName": "",
  "usesSDF": false,
}