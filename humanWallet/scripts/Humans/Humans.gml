enum HUMAN_STATES{
    IDLE,
    WALK,
    RUN,
    HUNGER,
    EAT,
    FALL_OUT,
    TAKED,
    DEAD,
    JUMP
}

enum HUMAN_NEED_STATES{
    DEAD ,
    BAD ,
    ACCEPT ,
    OK ,
    GOOD ,
    EXCELENT ,
}

function HumanNeed ( _state, _date, _countdown) constructor {
    state = _state;
    date_last_update = _date;
    countdown = _countdown;
    alert = false;
    
    function refresh_state (_load=false){
        var _gap_counter = countdown_gap_counter(date_last_update, countdown)
        show_debug_message(date_datetime_string(date_last_update)+ "  - " + string(countdown)+ "  - " + string(_gap_counter));
        
		if(_gap_counter){
			state = state - abs(_gap_counter);
			date_last_update = date_current_datetime();		
		}
	
		if(_load){
            if(state <= 0){
                state = HUMAN_NEED_STATES.BAD;
            }
        }
    }
}

/// Human Class
function Human(_id, _name) constructor {
    uid = _id;
    name = _name;
    state = HUMAN_STATES.IDLE;
    alert_happy = false;
    hungry = new HumanNeed(
                HUMAN_NEED_STATES.ACCEPT, 
                date_current_datetime(),
                30
            );
    
    function refresh_needs_states(_load){
        hungry.refresh_state(_load)
    }
    
    function get_id(){
        return string(uid)
    }
    
    function save () {
        ini_open("maps-joe.ini");
            ini_write_string(get_id(), "name", name);
            ini_write_real(get_id(), "state", state);
            ini_write_string(get_id(), "hungry", base64_encode(json_stringify(hungry)));
        ini_close();
    }
    
    function load (_id) {
        uid = _id;
        ini_open("maps-joe.ini");
            name = ini_read_string(get_id(), "name", name);
            state = ini_read_real(get_id(), "state", state);
            hungry = json_parse(base64_decode(ini_read_string(get_id(), "hungry", base64_encode(json_stringify(hungry)))));
        ini_close();
        show_debug_message(hungry)
        hungry = new HumanNeed(hungry.state,hungry.date_last_update, hungry.countdown)
		show_debug_message(hungry)
        refresh_needs_states(true)
    }
    
    function remove_data () {
        ini_open ("maps-joe.ini");
            ini_section_delete(get_id())
        ini_close ();
    }
}

/// HumanManager Class
function HumanManager(_max_humans) constructor {
    // Properties
    _limit_max_humans_alive_allowed = _max_humans;
    _current_humans_alive = 0;
    _total_humans_created = 0;
    _list_humans = []; // Array to store Human objects

    /// Create a human (respecting the limit)
    function _create_human() {
        if (_current_humans_alive <= _limit_max_humans_alive_allowed) {
            var _id = _total_humans_created;
            var _name = "joe_" + string(_id);
            var _new_human = new Human(_id, _name);

            array_push(_list_humans, _new_human);
            _total_humans_created++;
            _current_humans_alive++;
            return _new_human; // Return the created human
        } else {
            show_message("Maximum limit of humans reached.");
            return undefined; // Cannot create
        }
    }
    
    function create_human() {

        var cx = (camera_get_view_x(view_camera[0])+ (global.width_camara/2))
        var cy = (camera_get_view_y(view_camera[0]))
        var _human = _create_human();
        if (_human != undefined ){
            with(instance_create_layer(cx,cy, "Instances", _obj_human_padre)){
                human = _human
                human.save();
            }
            save();            
        }
    }
    
    function load_humans() {
        var cx = (camera_get_view_x(view_camera[0])+ (global.width_camara/2))
        var cy = (camera_get_view_y(view_camera[0]))
        for (var i = 0; i < array_length(_list_humans); i++) {
            with(instance_create_layer(cx,cy, "instances", _obj_human_padre)){
                human.load(other._list_humans[i].uid)
            } 
        }
    }

    /// Get a human by ID
    function get_human_by_id(_id) {
        for (var i = 0; i < array_length(_list_humans); i++) {
            if (_list_humans[i].uid == _id) {
                return _list_humans[i];
            }
        }
        return undefined; // Not found
    }

    /// Delete a human by ID
    function delete_human_by_id(_id) {
        for (var i = 0; i < array_length(_list_humans); i++) {
            if (_list_humans[i].uid == _id) {
                array_delete(_list_humans, i, 1); // Remove from array
                _current_humans_alive--;
                save();
                return true; // Success
            }
        }
        return false; // Not found
    }

    /// List all humans (optional, for debugging)
    function list_humans() {
        return _list_humans;
    }
    
    function list_humans_to_string(){
        return json_stringify(_list_humans);
    }
    
    // Save the humans to a file
    function save() {
        ini_open("maps-joe.ini");
            ini_write_string("human-data", "_list_humans", base64_encode(list_humans_to_string()));
            ini_write_real("human-data", "_current_humans_alive", _current_humans_alive);
            ini_write_real("human-data", "_total_humans_created", _total_humans_created);
        ini_close();
    }

    // Load the humans from a file
    function load() {
        ini_open("maps-joe.ini");
            _list_humans = json_parse(base64_decode(ini_read_string("human-data", "_list_humans", base64_encode(list_humans_to_string()))));
            _current_humans_alive = ini_read_real("human-data", "_current_humans_alive", _current_humans_alive);
            _total_humans_created = ini_read_real("human-data", "_total_humans_created", _total_humans_created);
        ini_close();
    }
}



