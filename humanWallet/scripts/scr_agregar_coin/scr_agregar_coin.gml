// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_agregar_coin(argument0){
	var valueOfCoin = argument0;
	
	obj_gui_store.total_coins += valueOfCoin ;
	#region CHECK DATA AND LOAD - OR CREATE NEW
		ini_open ("game_data.ini");		
			ini_write_real ("Store", "total_coins", obj_gui_store.total_coins );
		ini_close ();
	#endregion
	return 1; 

}