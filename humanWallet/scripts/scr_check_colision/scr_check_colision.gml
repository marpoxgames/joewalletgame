// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function script_check_colision(argument0, argument1) {

	//---------------------------------------------------------------------------------------
	//----------------------------------COLISIONES-----------------------------------------
	//---------------------------------------------------------------------------------------

	var intVX=argument0;

	var intVY=argument1;



	//Colision Horizontal 
	repeat(abs(intVX)){
		//Cuesta Arriba
		if(place_meeting(x  ,y,obj_block) && place_meeting(x  , y-1,obj_block) && place_meeting(x  , y-2,obj_block) && !place_meeting(x  , y-3,obj_block)){
			y-=3;
		}else if(place_meeting(x  ,y,obj_block) && place_meeting(x  , y-1,obj_block) && !place_meeting(x  , y-2,obj_block)){
			y-=2;
		}else if(place_meeting(x  ,y,obj_block) && !place_meeting(x  , y-1,obj_block)){
			y--;
		}
		//Cuesta Abajo
		if(!place_meeting(x  ,y,obj_block) && !place_meeting(x  , y+1,obj_block) && !place_meeting(x+sign(intVX),y+2,obj_block) && !place_meeting(x+sign(intVX),y+3,obj_block) && place_meeting(x+sign(intVX),y+4,obj_block)){
			y+=3;
		}else if(!place_meeting(x  ,y,obj_block) && !place_meeting(x  , y+1,obj_block) && !place_meeting(x+sign(intVX),y+2,obj_block) && place_meeting(x+sign(intVX),y+3,obj_block)){
			y+=2;
		}else if(!place_meeting(x  ,y,obj_block) && !place_meeting(x  , y+1,obj_block) && place_meeting(x+sign(intVX),y+2,obj_block)){
			y++;
		}
		//Colision Normal bloque
		if(!place_meeting(x   ,y, obj_block))
		{
		x += sign(intVX)*get_delta_time();
		}else{
		intVX =0;
		break;
	}};


	//Colision Vertical 
	repeat(abs(intVY)){
	if(place_meeting(x ,y, obj_block))
	{

			 intVY =0;
			 break;
	
	}else{ 
	     y += sign(intVY)*get_delta_time(); 
	}} 


}
