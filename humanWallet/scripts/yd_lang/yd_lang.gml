// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function yd_lang(_key) {
	// return locale sting for a given key 
	if(ds_map_exists(global.yd_locale_words,_key))
	{
		return ds_map_find_value(global.yd_locale_words,_key);
	
	}else{
		return "??MISSING TRANSLATION??";	
	}
}
