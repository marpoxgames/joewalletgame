// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_draw_black_gui_surface(_surface_alpha_color){
	
	var _surface_color = c_black;

	if(global.active_gui && active_gui_control){
		if(surface_exists(_active_surface)){
					var _cw = camera_get_view_width(view_camera[0]);
					var _ch = camera_get_view_height(view_camera[0]);
					var _cx = camera_get_view_x(view_camera[0]);
					var _cy = camera_get_view_y(view_camera[0]);

						surface_set_target(_active_surface);	
						draw_set_color(_surface_color);
						draw_set_alpha(_surface_alpha_color);
						
						draw_rectangle(0,0,_cw,_ch,0);
						
					
						
	
					//RESET
					draw_set_alpha(1);
					surface_reset_target();
					draw_surface(_active_surface,_cx,_cy);

		}

		if(!surface_exists(_active_surface)){

				var _cw = camera_get_view_width(view_camera[0]);
				var _ch = camera_get_view_height(view_camera[0]);


					_active_surface = surface_create(_cw,_ch);

					surface_set_target(_active_surface);
					draw_set_color(_surface_color)
					draw_set_alpha(_surface_alpha_color);
					draw_rectangle(0,0,_cw,_ch,0);

					surface_reset_target();


		}
	}
	
}

