// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_control_press_left(_coord_button_x, _coord_button_y, _coord_button_offset) {
    if (device_mouse_check_button_released(0, mb_left)) {
        var _mouse_x = device_mouse_x_to_gui(0);
        var _mouse_y = device_mouse_y_to_gui(0);

        // Check if the mouse is within the button bounds
        if (abs(_mouse_x - _coord_button_x) <= _coord_button_offset && 
            abs(_mouse_y - _coord_button_y) <= _coord_button_offset) {
            if(DEBUG_MOUSE){
				var _object_name = object_get_name(object_index);
				show_debug_message("left press ->" + _object_name)
			}
			return true;
        }
    }
    return false;
}

function scr_control_press_right(_coord_button_x, _coord_button_y, _coord_button_offset) {
    if (device_mouse_check_button_released(0, mb_right)) {
        var _mouse_x = device_mouse_x_to_gui(0);
        var _mouse_y = device_mouse_y_to_gui(0);

        // Check if the mouse is within the button bounds
        if (abs(_mouse_x - _coord_button_x) <= _coord_button_offset && 
            abs(_mouse_y - _coord_button_y) <= _coord_button_offset) {
            if(DEBUG_MOUSE){
                var _object_name = object_get_name(object_index);
                show_debug_message("right press ->" + _object_name)
            }
            return true;
        }
    }
    return false;
}

function scr_mouse_over_rectangule(_coord_button_x, _coord_button_y, _coord_button_offset) {
    if(point_in_rectangle(device_mouse_x(0),device_mouse_y(0),
        _coord_button_x-_coord_button_offset,_coord_button_y-_coord_button_offset,
        _coord_button_x+_coord_button_offset,_coord_button_y+_coord_button_offset)){
        if(DEBUG_MOUSE){
            var _object_name = object_get_name(object_index);
            show_debug_message("mouse over ->" + _object_name)
        }
        return true;
    }
    return false;
    
}
